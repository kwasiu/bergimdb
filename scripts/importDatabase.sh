#!/bin/bash

echo "Starting script!"
echo "Downloading files"
rm -rf imdb
mkdir imdb
wget --output-document="imdb/name.basics.tsv.gz" https://datasets.imdbws.com/name.basics.tsv.gz
wget --output-document="imdb/title.basics.tsv.gz" https://datasets.imdbws.com/title.basics.tsv.gz

gunzip imdb/name.basics.tsv.gz
gunzip imdb/title.basics.tsv.gz

echo "Remove first line"
sed '1d' imdb/title.basics.tsv > imdb/tmpfile
mv imdb/tmpfile imdb/title.basics.tsv

sed '1d' imdb/name.basics.tsv > imdb/tmpfile
mv imdb/tmpfile imdb/name.basics.tsv

echo "Execute movie script"
copyMovies="\\COPY movie (\"id\",\"type\",\"primary_title\",\"origin_title\",\"for_adults\",\"start_year\",\"end_year\",\"runtime_minutes\",\"genres\") FROM 'imdb/title.basics.tsv' WITH DELIMITER E'\t'"
psql -U postgres -d imdb -c "$copyMovies"

echo "Execute actor script"
copyActor="\\COPY actor (\"id\",\"name\",\"birth_year\",\"death_year\",\"professions\") FROM STDIN WITH DELIMITER E'\t'"
cut -f 1-5 imdb/name.basics.tsv | psql -U postgres -d imdb -c "$copyActor"

echo "Execute connection script"
copyActorMovie="\\COPY actor_movie (\"actor_id\",\"movie_id\") FROM STDIN WITH DELIMITER E'\t'"
sql=""
i=1
while IFS=$'\t' read -r v1 v2 v3 v4 v5 v6; do
	while read movieid; do
		if [ "$movieid" != "N" ] && [ "$movieid" != "" ]; then
			sql="$sql$v1\\t$movieid\\n"
			i=$((i+1))
		fi
	done <<< "$(echo -e $v6 | tr ',' '\n')"
	# echo -e "$sql" | sed '/^\s*$/d'
	if [ $i -ge 1000 ]; then
		echo -e "$sql" | sed '/^\s*$/d' | psql -U postgres -d imdb -c "$copyActorMovie"
		sql=""
		i=1
	fi
done < imdb/name.basics.tsv

rm -rf imdb
echo "Finishing script!"