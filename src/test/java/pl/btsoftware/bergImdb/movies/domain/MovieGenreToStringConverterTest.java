package pl.btsoftware.bergImdb.movies.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.btsoftware.bergImdb.movies.domain.MovieGenre.*;

public class MovieGenreToStringConverterTest {
    private MovieGenreToStringConverter movieGenreToStringConverter;

    @Before
    public void setUp() {
        movieGenreToStringConverter = new MovieGenreToStringConverter();
    }

    @Test
    public void shouldCreateSetOfGenres() {
        // given
        String genres = "crime,documentary,action";

        // when
        Set<MovieGenre> movieGenresSet = movieGenreToStringConverter.convertToEntityAttribute(genres);

        // then
        assertThat(movieGenresSet).containsExactlyInAnyOrder(ACTION, CRIME, DOCUMENTARY);
    }

    @Test
    public void shouldCreateStringWithMovieGenresSeparatedByComma() {
        // given
        Set<MovieGenre> movieGenreSet = new HashSet<>();
        movieGenreSet.add(ACTION);
        movieGenreSet.add(CRIME);
        movieGenreSet.add(DOCUMENTARY);

        // when
        String actorProfession = movieGenreToStringConverter.convertToDatabaseColumn(movieGenreSet);

        // then
        assertThat(actorProfession).containsIgnoringCase(ACTION.toString());
        assertThat(actorProfession).containsIgnoringCase(CRIME.toString());
        assertThat(actorProfession).containsIgnoringCase(DOCUMENTARY.toString());
    }
}