package pl.btsoftware.bergImdb.movies.domain;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.btsoftware.bergImdb.movies.domain.MovieType.VIDEO;

public class MovieTypeToStringConverterTest {

    private MovieTypeToStringConverter movieTypeToStringConverter;

    @Before
    public void setUp() {
        movieTypeToStringConverter = new MovieTypeToStringConverter();
    }

    @Test
    public void shouldCreateMovieType() {
        // given
        String movieType = "video";

        // when
        MovieType movieTypeEnum = movieTypeToStringConverter.convertToEntityAttribute(movieType);

        // then
        assertThat(movieTypeEnum).isEqualTo(VIDEO);
    }

    @Test
    public void shouldCreateStringWithMovieType() {
        // given
        MovieType movieTypeEnum = VIDEO;

        // when
        String movieType = movieTypeToStringConverter.convertToDatabaseColumn(movieTypeEnum);

        // then
        assertThat(movieType).isEqualTo("video");
    }
}