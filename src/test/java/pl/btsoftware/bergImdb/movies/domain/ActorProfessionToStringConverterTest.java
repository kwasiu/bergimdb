package pl.btsoftware.bergImdb.movies.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.btsoftware.bergImdb.movies.domain.ActorProfession.*;

public class ActorProfessionToStringConverterTest {

    private ActorProfessionToStringConverter actorProfessionToStringConverter;

    @Before
    public void setUp() {
        actorProfessionToStringConverter = new ActorProfessionToStringConverter();
    }

    @Test
    public void shouldCreateSetOfProfessions() {
        // given
        String professions = "actor,director,editor";

        // when
        Set<ActorProfession> actorProfessionSet = actorProfessionToStringConverter.convertToEntityAttribute(professions);

        // then
        assertThat(actorProfessionSet).containsExactlyInAnyOrder(ACTOR, DIRECTOR, EDITOR);
    }

    @Test
    public void shouldCreateStringWithProfessionsSeparatedByComma() {
        // given
        Set<ActorProfession> actorProfessionSet = new HashSet<>();
        actorProfessionSet.add(ACTOR);
        actorProfessionSet.add(DIRECTOR);
        actorProfessionSet.add(EDITOR);

        // when
        String actorProfession = actorProfessionToStringConverter.convertToDatabaseColumn(actorProfessionSet);

        // then
        assertThat(actorProfession).containsIgnoringCase(ACTOR.toString());
        assertThat(actorProfession).containsIgnoringCase(DIRECTOR.toString());
        assertThat(actorProfession).containsIgnoringCase(EDITOR.toString());
    }
}