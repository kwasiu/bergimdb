package pl.btsoftware.bergImdb.movies.application;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import pl.btsoftware.bergImdb.movies.domain.Actor;
import pl.btsoftware.bergImdb.movies.domain.Movie;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MovieServiceTest {
    private MovieRepository movieRepository;
    private MovieService movieService;

    @Before
    public void setUp() {
        movieRepository = mock(MovieRepository.class);
        movieService = spy(new MovieService(movieRepository));
    }

    @Test
    public void shouldCallFindAllFromActorRepositoryOnce() {
        // given
        Integer startYear = 1894;
        String genre = "Sport";
        Pageable pageable = new PageRequest(1, 1);

        // when
        movieService.findByParametersWithPagination(startYear, genre, pageable);

        // then
        verify(movieRepository, times(1)).findAll(ArgumentMatchers.<Specification<Movie>> any(), any(Pageable.class));
    }

}