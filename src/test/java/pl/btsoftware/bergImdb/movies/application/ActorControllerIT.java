package pl.btsoftware.bergImdb.movies.application;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import pl.btsoftware.bergImdb.movies.domain.*;
import pl.btsoftware.bergImdb.utils.RestConfig;
import pl.btsoftware.bergImdb.utils.RestPage;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActorControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldReturnFullInitializedActorWithMovies() throws Exception {
        // given
        String name = "Kayser";

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.ACTOR).param("name", name))
                .andExpect(status().isOk())
                .andReturn();

        // then
        Actor actor = createActorObjectWithNameCharlesKayser();

        RestPage<Actor> pageActor = convertMvcResultToActorPage(result);
        assertThat(pageActor.getContent()).hasSize(1);
        assertThat(pageActor.getContent()).containsExactly(actor);
    }

    private Actor createActorObjectWithNameCharlesKayser() {
        Set<MovieGenre> genres = new HashSet<>();
        genres.add(MovieGenre.COMEDY);
        genres.add(MovieGenre.SHORT);

        Movie movie = new Movie("tt0000005", MovieType.SHORT, "Blacksmith Scene", "Blacksmith Scene", false, 1893, null, 1, genres, "Comedy,Short", null);

        return new Actor("nm0443482", "Charles Kayser", 1878, 1966, Collections.singleton(ActorProfession.ACTOR), Collections.singleton(movie));
    }

    @Test
    public void shouldReturnLimitedNumberOfActors() throws Exception {
        // given
        String name = "Pappé";
        String page = "1";
        String limit = "1";

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.ACTOR).param("name", name).param("page", page).param("size", limit))
                .andExpect(status().isOk())
                .andReturn();

        // then
        RestPage<Actor> pageActor = convertMvcResultToActorPage(result);
        assertThat(pageActor.getNumberOfElements()).isEqualTo(Integer.valueOf(limit));
        assertThat(pageActor.getContent()).hasSize(Integer.valueOf(limit));
        assertThat(pageActor.getPageable().getPageNumber()).isEqualTo(Integer.valueOf(page));
    }

    private RestPage<Actor> convertMvcResultToActorPage(MvcResult response) throws IOException {
        String jsonResponse = response.getResponse().getContentAsString();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(RestPage.class, Actor.class);
        return objectMapper.readValue(jsonResponse, type);
    }
}