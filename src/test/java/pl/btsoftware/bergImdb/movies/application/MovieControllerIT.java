package pl.btsoftware.bergImdb.movies.application;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import pl.btsoftware.bergImdb.movies.domain.*;
import pl.btsoftware.bergImdb.utils.RestConfig;
import pl.btsoftware.bergImdb.utils.RestPage;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldReturnTotalNumberOfItems6And6MovieNamesWhenStartYearIs1894() throws Exception {
        // given
        String startYear = "1894";

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.MOVIE).param("startYear", startYear))
                .andExpect(status().isOk())
                .andReturn();

        // then
        RestPage<Movie> pageMovie = convertMvcResultToMoviePage(result);
        assertThat(pageMovie.getNumberOfElements()).isEqualTo(6);
        assertThat(pageMovie.getContent()).hasSize(6);
        assertThat(pageMovie.getContent()).extracting("primaryTitle")
                .containsExactlyInAnyOrder("Carmencita", "Chinese Opium Den", "Corbett and Courtney Before the Kinetograph", "Edison Kinetoscopic Record of a Sneeze", "Miss Jerry", "Autour d'une cabine");
    }

    @Test
    public void shouldReturnTotalNumberOfItems2AndMovieNamesWhenStartYearIs1894AndGenreDocumentary() throws Exception {
        // given
        String startYear = "1894";
        MovieGenre genre = MovieGenre.DOCUMENTARY;

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.MOVIE).param("startYear", startYear).param("genre", genre.toString()))
                .andExpect(status().isOk())
                .andReturn();

        // then
        RestPage<Movie> pageMovie = convertMvcResultToMoviePage(result);
        assertThat(pageMovie.getNumberOfElements()).isEqualTo(2);
        assertThat(pageMovie.getContent()).hasSize(2);
        assertThat(pageMovie.getContent()).extracting("primaryTitle")
                .containsExactlyInAnyOrder("Carmencita", "Edison Kinetoscopic Record of a Sneeze");
    }

    @Test
    public void shouldReturnLimitedNumberOfMovies() throws Exception {
        // given
        String startYear = "1894";
        String page = "1";
        String limit = "3";

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.MOVIE).param("startYear", startYear).param("page", page).param("size", limit))
                .andExpect(status().isOk())
                .andReturn();

        // then
        RestPage<Movie> pageMovie = convertMvcResultToMoviePage(result);
        assertThat(pageMovie.getNumberOfElements()).isEqualTo(Integer.valueOf(limit));
        assertThat(pageMovie.getContent()).hasSize(Integer.valueOf(limit));
        assertThat(pageMovie.getPageable().getPageNumber()).isEqualTo(Integer.valueOf(page));
    }

    @Test
    public void shouldReturnFullInitializedMovieWithActors() throws Exception {
        // given
        String startYear = "1896";

        // when
        MvcResult result = mockMvc.perform(get(RestConfig.MOVIE).param("startYear", startYear))
                .andExpect(status().isOk())
                .andReturn();

        // then
        RestPage<Movie> pageMovie = convertMvcResultToMoviePage(result);
        assertThat(pageMovie.getTotalElements()).isEqualTo(1);

        Movie movie = createMovieWithTitleTheArrivalOfATrain();

        assertThat(pageMovie.getContent()).hasSize(1);
        assertThat(pageMovie.getContent()).containsExactly(movie);
    }

    private Movie createMovieWithTitleTheArrivalOfATrain() {
        Set<MovieGenre> genreSet = new HashSet<>();
        genreSet.add(MovieGenre.DOCUMENTARY);
        genreSet.add(MovieGenre.SHORT);

        Set<ActorProfession> actorProfessionSet = new HashSet<>();
        actorProfessionSet.add(ActorProfession.PRODUCER);
        actorProfessionSet.add(ActorProfession.DIRECTOR);
        actorProfessionSet.add(ActorProfession.ACTOR);

        Set<Actor> actorSet = new HashSet<>();
        actorSet.add(new Actor("nm0525908", "Auguste Lumière", 1862, 1954, actorProfessionSet, null));

        return new Movie("tt0000012", MovieType.SHORT, "The Arrival of a Train", "L'arrivée d'un train à La Ciotat", false, 1896, 1897, 1, genreSet, "Documentary,Short", actorSet);

    }

    private RestPage<Movie> convertMvcResultToMoviePage(MvcResult response) throws IOException {
        String jsonResponse = response.getResponse().getContentAsString();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(RestPage.class, Movie.class);
        return objectMapper.readValue(jsonResponse, type);
    }
}