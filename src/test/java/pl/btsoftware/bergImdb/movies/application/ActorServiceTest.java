package pl.btsoftware.bergImdb.movies.application;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import pl.btsoftware.bergImdb.movies.domain.Actor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ActorServiceTest {

    private ActorRepository actorRepository;
    private ActorService actorService;

    @Before
    public void setUp() {
        actorRepository = mock(ActorRepository.class);
        actorService = spy(new ActorService(actorRepository));
    }

    @Test
    public void shouldCallFindAllFromActorRepositoryOnce() {
        // given
        String name = "Tom Hanks";
        Pageable pageable = new PageRequest(1, 1);

        // when
        actorService.findByParametersWithPagination(name, pageable);

        // then
        verify(actorRepository, times(1)).findAll(ArgumentMatchers.<Specification<Actor>> any(), any(Pageable.class));
    }
}