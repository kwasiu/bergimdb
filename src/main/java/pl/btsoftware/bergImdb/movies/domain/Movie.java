package pl.btsoftware.bergImdb.movies.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Movie {

    @Id
    @GeneratedValue
    private String id;
    @Convert(converter = MovieTypeToStringConverter.class)
    private MovieType type;
    @Column(length = 500)
    private String primaryTitle;
    @Column(length = 500)
    private String originTitle;
    private boolean forAdults;
    private Integer startYear;
    private Integer endYear;
    private Integer runtimeMinutes;
    @Convert(converter = MovieGenreToStringConverter.class)
    private Set<MovieGenre> genres;
    @Column(name = "genres", updatable = false, insertable = false)
    private String genresString;
    @JsonIgnoreProperties({"movies"})
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "actor_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    private Set<Actor> actors = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return forAdults == movie.forAdults &&
                Objects.equals(id, movie.id) &&
                type == movie.type &&
                Objects.equals(primaryTitle, movie.primaryTitle) &&
                Objects.equals(originTitle, movie.originTitle) &&
                Objects.equals(startYear, movie.startYear) &&
                Objects.equals(endYear, movie.endYear) &&
                Objects.equals(runtimeMinutes, movie.runtimeMinutes) &&
                Objects.equals(genres, movie.genres) &&
                Objects.equals(genresString, movie.genresString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, primaryTitle, originTitle, forAdults, startYear, endYear, runtimeMinutes, genres, genresString);
    }
}
