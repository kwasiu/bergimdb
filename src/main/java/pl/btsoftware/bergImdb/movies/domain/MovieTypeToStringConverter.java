package pl.btsoftware.bergImdb.movies.domain;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MovieTypeToStringConverter implements AttributeConverter<MovieType, String> {

    @Override
    public String convertToDatabaseColumn(MovieType attribute) {
        return attribute.toString().toLowerCase();
    }

    @Override
    public MovieType convertToEntityAttribute(String dbData) {
        if(StringUtils.isEmpty(dbData)) {
            return null;
        }
        return MovieType.valueOf(dbData.toUpperCase());
    }
}
