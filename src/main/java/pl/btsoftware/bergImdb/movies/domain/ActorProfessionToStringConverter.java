package pl.btsoftware.bergImdb.movies.domain;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class ActorProfessionToStringConverter implements AttributeConverter<Set<ActorProfession>, String> {
    private final String DELIMITER = ",";

    @Override
    public String convertToDatabaseColumn(Set<ActorProfession> attribute) {
        return attribute.stream().map(Enum::toString).collect(Collectors.joining(DELIMITER));
    }

    @Override
    public Set<ActorProfession> convertToEntityAttribute(String dbData) {
        if (StringUtils.isEmpty(dbData)) {
            return new HashSet<>();
        }

        try (Stream<String> stream = Arrays.stream(dbData.split(DELIMITER))) {
            return stream.map(String::toUpperCase).map(ActorProfession::valueOf).collect(Collectors.toSet());
        }
    }
}
