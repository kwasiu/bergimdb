package pl.btsoftware.bergImdb.movies.domain;

public enum MovieGenre {
    ACTION,
    ADULT,
    ADVENTURE,
    ANIMATION,
    BIOGRAPHY,
    COMEDY,
    CRIME,
    DOCUMENTARY,
    DRAMA,
    FAMILY,
    FANTASY,
    FILM_NOIR,
    GAME_SHOW,
    HISTORY,
    HORROR,
    MUSIC,
    MUSICAL,
    MYSTERY,
    NEWS,
    REALITY_TV,
    ROMANCE,
    SCI_FI,
    SHORT,
    SPORT,
    TALK_SHOW,
    THRILLER,
    WAR,
    WESTERN;

    public static MovieGenre findGenreByName(String genre) {
        switch (genre) {
            case "SCI-FI":
                return MovieGenre.SCI_FI;
            case "GAME-SHOW":
                return MovieGenre.GAME_SHOW;
            case "TALK-SHOW":
                return MovieGenre.TALK_SHOW;
            case "REALITY-TV":
                return MovieGenre.REALITY_TV;
            case "FILM-NOIR":
                return MovieGenre.FILM_NOIR;
            default:
                return MovieGenre.valueOf(genre);
        }
    }
}
