package pl.btsoftware.bergImdb.movies.domain;

public enum MovieType {
    SHORT,
    MOVIE,
    TVSERIES,
    TVEPISODE,
    VIDEO,
    TVMOVIE,
    TVSPECIAL,
    VIDEOGAME,
    TVMINISERIES,
    TVSHORT;
}
