package pl.btsoftware.bergImdb.movies.domain;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Converter
public class MovieGenreToStringConverter implements AttributeConverter<Set<MovieGenre>, String> {
    private final String DELIMITER = ",";

    @Override
    public String convertToDatabaseColumn(Set<MovieGenre> attribute) {
        return attribute.stream().map(Enum::toString).map(s -> s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase()).collect(Collectors.joining(DELIMITER));
    }

    @Override
    public Set<MovieGenre> convertToEntityAttribute(String dbData) {
        if (StringUtils.isEmpty(dbData)) {
            return new HashSet<>();
        }

        try (Stream<String> stream = Arrays.stream(dbData.split(DELIMITER))) {
            return stream.map(String::toUpperCase).map(MovieGenre::findGenreByName).collect(Collectors.toSet());
        }
    }
}
