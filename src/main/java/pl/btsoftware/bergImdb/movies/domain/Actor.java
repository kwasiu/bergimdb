package pl.btsoftware.bergImdb.movies.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Actor {

    @Id
    @GeneratedValue
    private String id;
    private String name;
    private Integer birthYear;
    private Integer deathYear;
    @Convert(converter = ActorProfessionToStringConverter.class)
    private Set<ActorProfession> professions;
    @JsonIgnoreProperties({"actors"})
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "actors")
    private Set<Movie> movies = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Actor actor = (Actor) o;
        return Objects.equals(id, actor.id) &&
                Objects.equals(name, actor.name) &&
                Objects.equals(birthYear, actor.birthYear) &&
                Objects.equals(deathYear, actor.deathYear) &&
                Objects.equals(professions, actor.professions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthYear, deathYear, professions);
    }
}
