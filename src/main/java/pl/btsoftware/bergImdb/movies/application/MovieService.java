package pl.btsoftware.bergImdb.movies.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.btsoftware.bergImdb.movies.domain.Movie;

@Service
@Slf4j
class MovieService {
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Transactional(readOnly = true)
    Page<Movie> findByParametersWithPagination(Integer startYear, String genre, Pageable pageable) {
        Specification<Movie> spec = Specifications.where(new MovieWithStartYearEquals(startYear))
                .and(new MovieWithGenreLike(genre));

        return movieRepository.findAll(spec, pageable);
    }
}
