package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.btsoftware.bergImdb.movies.domain.Movie;

import static pl.btsoftware.bergImdb.utils.RestConfig.MOVIE;

@RestController
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = MOVIE)
    public Page<Movie> findMovieByParameters(@RequestParam(required = false) Integer startYear,
                                             @RequestParam(required = false) String genre,
                                             Pageable pageable) {
        return movieService.findByParametersWithPagination(startYear, genre, pageable);
    }
}
