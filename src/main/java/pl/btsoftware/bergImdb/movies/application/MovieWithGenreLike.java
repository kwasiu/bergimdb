package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.jpa.domain.Specification;
import pl.btsoftware.bergImdb.movies.domain.Movie;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

class MovieWithGenreLike implements Specification<Movie> {
    private final String genre;

    MovieWithGenreLike(String genre) {
        this.genre = genre;
    }

    @Override
    public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (genre == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }
        // https://hibernate.atlassian.net/browse/HHH-9991
        return criteriaBuilder.like(criteriaBuilder.upper(root.get("genresString")), "%" + genre.toUpperCase() + "%");
    }
}
