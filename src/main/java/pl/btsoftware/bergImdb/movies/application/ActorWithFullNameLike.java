package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.jpa.domain.Specification;
import pl.btsoftware.bergImdb.movies.domain.Actor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

class ActorWithFullNameLike implements Specification<Actor> {
    private final String fullName;

    ActorWithFullNameLike(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (fullName == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }

        return criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), "%" + fullName.toUpperCase() + "%");
    }
}
