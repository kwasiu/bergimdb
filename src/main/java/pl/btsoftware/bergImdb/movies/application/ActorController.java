package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.btsoftware.bergImdb.movies.domain.Actor;

import static pl.btsoftware.bergImdb.utils.RestConfig.ACTOR;

@RestController
public class ActorController {

    private final ActorService actorService;

    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    @GetMapping(value = ACTOR)
    public Page<Actor> findActorByName(@RequestParam String name,
                                       Pageable pageable) {
        return actorService.findByParametersWithPagination(name, pageable);
    }
}
