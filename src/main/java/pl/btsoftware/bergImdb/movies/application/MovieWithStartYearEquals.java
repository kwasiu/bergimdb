package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.jpa.domain.Specification;
import pl.btsoftware.bergImdb.movies.domain.Movie;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

class MovieWithStartYearEquals implements Specification<Movie> {
    private final Integer startYear;

    MovieWithStartYearEquals(Integer startYear) {
        this.startYear = startYear;
    }

    @Override
    public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if(startYear == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        }
        return criteriaBuilder.equal(root.get("startYear"), startYear);
    }
}
