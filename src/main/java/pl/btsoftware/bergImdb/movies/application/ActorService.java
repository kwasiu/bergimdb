package pl.btsoftware.bergImdb.movies.application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.btsoftware.bergImdb.movies.domain.Actor;

@Service
class ActorService {

    private final ActorRepository actorRepository;

    public ActorService(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    @Transactional(readOnly = true)
    Page<Actor> findByParametersWithPagination(String fullName, Pageable pageable) {
        Specification<Actor> spec = Specifications.where(new ActorWithFullNameLike(fullName));

        return actorRepository.findAll(spec, pageable);
    }
}
