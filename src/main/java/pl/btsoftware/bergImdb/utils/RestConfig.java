package pl.btsoftware.bergImdb.utils;

public class RestConfig {
    public static final String MOVIE = "/movie";
    public static final String ACTOR = "/actor";
}
