package pl.btsoftware.bergImdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableSpringDataWebSupport
public class BergImdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(BergImdbApplication.class, args);
	}
}
